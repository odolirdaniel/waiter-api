/**
 * View Models used by Spring MVC REST controllers.
 */
package panthro.web.rest.vm;
